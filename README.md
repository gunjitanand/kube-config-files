Create a below folder on server reference for centralized logging and mysql persistent data:

•	/mnt/mysqldata

•	/applogs

**In order to deployment mysql**:


 kubectl apply -f mysql-secret.yaml

 kubectl apply -f mysql-pv.yaml

 kubectl apply -f mysql-deployment.yaml


**To have rabbitMQ up and running kindly execute below commands in K8 cluster:**


 kubectl apply -f rabbitmq-secret.yaml

 kubectl apply -f rabbitmq-deployment.yaml

**Setup configuration via configmap:**


 kubectl apply -f configmap.yaml


**To deploy microservices in cluster**:

 kubectl apply -f subscription-deployment.yaml

 kubectl apply -f authentication-deployment.yaml
 
 kubectl apply -f notification-deployment.yaml

**To expose authentication service to frontend, create ingress**:

 kubectl apply -f authentication-ingress.yaml 


